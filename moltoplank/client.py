from sys import argv
from .io import construct_dataset, convert_mtd


def run(help_arg, func, kw_options):
    """Parse the arguments and display help message like a classic terminal
    interface.

    Args:
        help_arg (func): A function that will be call if --help is passed in
            argument.
        func (func): The function that will be used if the right argument are
            given.
        kw_options (dict(str: *)): A dictionnary that will contains the default
            arguments to pass to the function.

    The behaviour is the following:
        func needs a string as first argument plus a bunch of others parameters
        thoses parameters can be given in two ways.
        In sequential order:
            The different arguments will be link the the different keyword and
            convert in the right type.
        In --{option_name} value:
            The option_name will be used as a key and the value will be convert
            in the right type.
        Only the original keywords will be passed as input to the function.
        And the options will be given to the function as a tuple of the values
        in the dictionnary like so:
            ```func(path, *tuple(options[k] for k in kw))```
        The order of the values in the dictionnary are therefore very important
        be careful.
    """
    execute = False
    kw = list(kw_options.keys())
    types = {k: type(kw_options[k]) for k in kw}
    options = kw_options
    match argv:
        case [_]:
            print("No argument given try to use\n\t --help")
        case [_, "--help", *trash]:
            help_arg()
        case [_, path]:
            execute = True
        case [_, path, *optional]:
            execute = True

            j = 0
            key = None
            for opt in optional:
                if opt[:2] == "--":
                    key = opt[2:]
                elif key:
                    if key in kw:
                        options[key] = types[key](opt)
                    else:
                        print(f"No --{key} parameter exist:\n\tskipped.")
                    key = None
                else:
                    options[kw[j]] = types[kw[j]](opt)
                    j += 1

    if execute:
        func(path, *tuple(options[k] for k in kw))


def mtp_help():
    print(
        """                                                          
            moltoplank input_path [output_file]                        
                                                                     
            Args:                                                    
                input_path (string):                                 
                    Valid path to a folder containing .out files from
                    Quantum Espresso's Pw.                           
                                                                     
            Optional: --{key} 
                output_file (string): --output                               
                    Path and name of the parquet file for the output.
                    If {output_file}.parquet already exist it will be
                    overwritten.                                     
          """
    )


def mtd_help():
    print(
        """                                                          
            moltodp input_file [output_folder size random]

            Args:
                input_path (string):
                    Valid path to a folder containing Quantum Espresso 
                    Pw.x MD output.

            Optional: --{key}
                output_file (string): --output
                    Path for the output. If it already exist it will be
                    overwritten.
                    default: dataset
                size_chunk (int): --size
                    Size of the chunk in terms of number of configurations.
                    default: 1000
                random (1 or 0): --random
                    If the element of the chunk should be choosen randomly.
                    default: 1 (True)
          """
    )


def run_mtp():
    default = {"output": "dataset"}
    run(mtp_help, construct_dataset, default)


def run_mtd():
    default = {"output": "dataset", "size": 1000, "random": 1}
    run(mtd_help, convert_mtd, default)
