from glob import glob
from pathlib import Path as pl
from pwtools.parse import PwMDOutputFile

import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import pyarrow.dataset as ds

GPa2eVA3 = 1 / 160.21766208


def progress(i, N):
    """Show progress for the treatment of the data.

    Args:
        i (int): Current value.
        N (int): Final value.
    """
    i += 1
    n = int(30 * i / N)
    print(f"|{'=' * n}>{' ' * (30 - n)}| file{i: 5d}/{N}", end="\r")


def pre_reading(ipath):
    """Start to gather informations on the .out files.

    Args:
        ipath (str): Input path of the folder containing the out files.

    Returns:
        np.array(str): Every species encounter in the files.
        list(PwMDOutputFile): A list of the files ready to be parsed.
    """
    # We recover the files in the folder and construct the list of the species.
    files = glob(f"{ipath}/*.out")
    N = len(files)

    species = []
    PwMDs = []
    for i, path in enumerate(files):
        progress(i, N)
        PwMD = PwMDOutputFile(path)
        for s in np.unique(PwMD.get_symbols()):
            species.append(s)
        PwMDs.append(PwMD)

    species = np.unique(species)
    print("\n", species)

    return species, PwMDs


def gen_schema(species):
    """Generate the schema of the parquet data file.

    Args:
        species (list(str)): List of the kind of species in the out files.

    Returns:
        pa.schema: schema of the data structure
    """
    # Primitives:
    double = pa.float64()
    list_str = pa.list_(pa.string())
    list_double = pa.list_(double)

    # Structures
    training = pa.struct(
        [
            ("a", list_str),
            ("x", list_double),
            ("y", list_double),
            ("z", list_double),
            ("Fx", list_double),
            ("Fy", list_double),
            ("Fz", list_double),
            ("etot", double),
        ]
    )
    criterion = [(f"d{s}", double) for s in species]
    criterion += [("error", double), ("etot", double)]
    criterion = pa.struct(criterion)

    # schema of the data structure
    return pa.schema([("training", training), ("criterion", criterion)])


def traj_parse(PwMD, species, schema):
    """Parse the trajectory into the structure of the parquet.

    Args:
        PwMD (pwtools.parse.PwMDOutputFile): Quantum Espresso's output.
        species (list(str)): List of the kind of species in the out files.
        schema (pa.schema): schema of the data structure

    Returns:
        pa.table: A parsed trajectory in a pyarrow table.
    """
    # We recover the pw.x output
    MD = PwMD.get_traj()

    # We create a bunch of masks in order to get the positions by species
    symbols = PwMD.get_symbols()
    masks = [np.array(symbols) == s for s in species]

    # We compute the mean position of each atoms during the dynamics
    mean_pos = MD.coords.mean(0)

    # We compute the displacement from the mean postions at each frame
    dm = np.sqrt(np.power(mean_pos - MD.coords, 2).sum(2))

    # We create a dictionary of the mean displacement of every species,
    # for all frames.
    dm = {f"d{s}": dm[:, mask].mean(1) for s, mask in zip(species, masks)}

    # We add the error and the energy in the dictionary
    dm["error"] = np.zeros(MD.get_nstep())
    dm["etot"] = MD.get_etot()

    # We construct our structure as a list of dictionnaries
    struct_1 = []
    struct_2 = []
    for i, md in enumerate(MD):
        # For all the frames we get the positions and the forces
        x, y, z = md.coords.T
        fx, fy, fz = md.forces.T

        # And we append the differents structures
        struct_1.append(
            {
                "a": symbols,
                "x": x,
                "y": y,
                "z": z,
                "Fx": fx,
                "Fy": fy,
                "Fz": fz,
                "etot": dm["etot"][i],
            }
        )
        struct_2.append({k: dm[k][i] for k in dm})

    return pa.table([struct_1, struct_2], schema=schema)


def construct_dataset(ipath, ofile):
    """given an input path and an output path, it will parse
    the Quantum Espresso's Pw out file into a parquet file.

    Args:
        ipath (str): input path of the folder containing the out files
        ofile (str): Path and name of the parquet output
    
    We construct our database and thus the data structure and types.         
    The structure is the following :
     +--------------------+
    +--------------------+ \
    | Configuration n°(i) \ +-------------------------------------------------+
    |                      +-------------------------------------------------+|
    | +--------------+  One configuration is composed by two structures :    ||
    | | Training data \ The first one contains the training data.            ||
    | |                +---------------+ The second one contains the         ||
    | | Kind of atom    : list(string) | criterion that could be used        ||
    | | X positions [Å] : list(double) | for choosing the configuration.     ||
    | | Y positions [Å] : list(double) | +-------------------------+         ||
    | | Z positions [Å] : list(double) | | Criterion data (doubles) \        ||
    | | X Forces [eV/Å] : list(double) | |                           +-----+ ||
    | | Y Forces [eV/Å] : list(double) | | Displacement by species    [Å]  | ||
    | | Z Forces [eV/Å] : list(double) | | Error on the Energy       [ev]  | ||
    | | Energy     [eV] : double       | | Energy (duplicata)        [eV]  | ||
    | +--------------------------------+ +---------------------------------+ |+
    +------------------------------------------------------------------------+
    """

    species, PwMDs = pre_reading(ipath)
    schema = gen_schema(species)

    N = len(PwMDs)
    # Output parquet file
    with pq.ParquetWriter(f"{ofile}.parquet", schema=schema) as writer:
        for k, PwMD in enumerate(PwMDs):
            progress(k, N)
            writer.write_table(traj_parse(PwMD, species, schema))
    print("\n")


def convert_mtd(ipath, opath, size, random):
    """
    Convert folder containing Quantum Espresso .out into dpdata structure

    Args:
        ipath (str):
            Input path folder containing QE .out files to be converted.
        opath (str):
            Output path where the data will be exported.
        size (int):
            Maximum number of configurations by chunk.
        random (int):
            If the configurations should be choosen randomly or not.

    The dpdata struture is the following:

    +--------------------+ 
    |       opath         \
    |                      +---------+
    |  +------------------+          |
    | +------------------+ \         |
    | |      pw.out       \ +------+ |
    | |                    +-----+ | |
    | | type_map.raw             | | |
    | | type.raw                 | | |
    | |                          | | |
    | | +---------+              | | |
    | | | set.{i}  \             | | |
    | | |           +--+         | | |
    | | | box.npy      |         | | |
    | | | coord.npy    |   ...   | | |
    | | | energy.npy   |         | | |
    | | | force.npy    |         | | |
    | | | virial.npy   |         | | |
    | | +--------------+         | + |
    | +--------------------------+   |
    +--------------------------------+
    """
    species, PwMDs = pre_reading(ipath)

    N = len(PwMDs)
    for k, PwMD in enumerate(PwMDs):
        progress(k, N)

        folder_name = ".".join(PwMD.filename.split("/")[-1].split(".")[:-1])
        file = f"{opath}/{folder_name}"
        pl(file).mkdir(parents=True, exist_ok=True)

        # We get the uniques species, the index where they first appear
        # and the inverse array.
        spec, ind, inv = np.unique(
            PwMD.get_symbols(), return_index=True, return_inverse=True
        )

        # We want the species and the inverse array to be in the order of the
        # first apearing in the symbols.
        sort = np.argsort(ind)

        # Now that we have the index in the order of the first apearance we
        # can sort our species and get the inverse array.
        spec = spec[sort]
        inv = np.argsort(sort)[inv]

        # We save thoses.
        np.savetxt(f"{file}/type_map.raw", spec, fmt="%s")
        np.savetxt(f"{file}/type.raw", inv, fmt="%s")

        # We get the trajectory.
        MD = PwMD.get_traj()
        Ns = MD.nstep

        # We get the relevant data.
        box = MD.get_cell()
        coord = MD.get_coords()
        energy = MD.get_etot()
        force = MD.get_forces()
        # Convert virial from pressure to energy [eV].
        virial = (
            MD.get_stress() * GPa2eVA3 * np.linalg.det(box)[:, np.newaxis, np.newaxis]
        )

        rbox = np.linalg.inv(box)
        coord = ((coord @ rbox)%1) @ box
        def save(i, get):
            n = get.size
            pl(f"{file}/set.{i:03d}").mkdir(parents=True, exist_ok=True)
            np.save(f"{file}/set.{i:03d}/box.npy", box[get].reshape(n, -1))
            np.save(f"{file}/set.{i:03d}/coord.npy", coord[get].reshape(n, -1))
            np.save(f"{file}/set.{i:03d}/energy.npy", energy[get].reshape(n, -1))
            np.save(f"{file}/set.{i:03d}/force.npy", force[get].reshape(n, -1))
            np.save(f"{file}/set.{i:03d}/virial.npy", virial[get].reshape(n, -1))

        Nbsh = int(Ns // size)
        elem = set(range(energy.size))
        for i in range(Nbsh):
            if random:
                get = np.random.choice(list(elem), size, replace=False)
            else:
                get = np.arange(i * size, (i + 1) * size)
            elem -= set(get)
            save(i, get.astype("int64"))

        get = np.array(list(elem))
        if not random:
            get.sort()
        save(Nbsh, get.astype("int64"))
