![MoltoPlank logo](doc/img/MtP_logo.svg)

# MoltoPlank

### Name
***\\ˈmɔl.to ˈplæŋk\\***
1. A lot of planks
2. Molecular to Parquet

Tool to convert Quantum Espresso's PW molecular dynamics to parquet dataset.

```bash
moltoplank input_path [output_path]
```

There is also a tool to convert Quantum Espresso's PW molecular dynamics to 
deepmd dataset aka *dpdata*.

To use it :

```bash
moltodp input_path [output_path] [size_of_sets] [random]
```

## Data Structure of moltoplank

```
The structure is the following :                                           
 +--------------------+                                                    
+--------------------+ \                                                   
| Configuration n°(i) \ +-------------------------------------------------+ 
|                      +-------------------------------------------------+|
| +--------------+  One configuration is composed by two structures :    ||
| | Training data \ The first one contains the training data.            ||
| |                +---------------+ The second one contains the         ||
| | Kind of atom    : list(string) | criterion that could be used        ||
| | X positions [Å] : list(double) | for choosing the configuration.     ||
| | Y positions [Å] : list(double) | +-------------------------+         ||
| | Z positions [Å] : list(double) | | Criterion data (doubles) \        ||
| | X Forces [eV/Å] : list(double) | |                           +-----+ ||
| | Y Forces [eV/Å] : list(double) | | Displacement by species    [Å]  | ||
| | Z Forces [eV/Å] : list(double) | | Error on the Energy       [ev]  | ||
| | Energy     [eV] : double       | | Energy (duplicata)        [eV]  | ||
| +--------------------------------+ +---------------------------------+ |+
+------------------------------------------------------------------------+  
```

## Install

```bash
pip install git+https://gitlab.com/Pacidus/moltoplank
```
